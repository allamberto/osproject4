// OS Project 4

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define BUCKETS 30000 

// Struct for packets
typedef struct __packet_holder {
    char isValid;           // 0 if no, 1 if yes
    char data[2400];        // The actual packet data
    uint32_t hash;          // Hash of the packet contents
} PacketHolder;

int main(int argc, char* argv[]) {
    // Allocate memory on heap for hash table 
    PacketHolder** packetCache = calloc(BUCKETS, sizeof(PacketHolder*));
    if (!packetCache) {
        printf("%s: Calloc failed. Unable to create cache.\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // INPUT VARIABLE
    PacketHolder* ph = calloc(1, sizeof(PacketHolder));
    ph->isValid = 0;
    sprintf(ph->data, "%i", 4);
    ph->hash = 214292051;

    // Check if packet is redundant
    if (isRedundant(packetCache, ph)) {
        printf("In cache.\n");
    }
    else {
        printf("Not in cache.\n");
    }
    int i;
    // Free allocated memory
    for (i = 0; i < BUCKETS; i++) {
        if (packetCache[i]) {
            free(packetCache[i]);
        }
    }
    free(packetCache);
    return 0;
}

// Function to check if packet is in cache
int isRedundant(PacketHolder** packetCache, PacketHolder* ph) {
    printf("DATA: %s, HASH: %i\n", ph->data, ph->hash);
    // Find cache entry to check using hash
    int bucket = ph->hash % BUCKETS;
    printf("BUCKET: %i\n", bucket);
    if (packetCache[bucket] && packetCache[bucket]->isValid) {
        // If compared data matches, return true
        if (!memcmp(ph->data, packetCache[bucket]->data, 2400)) {
            return 1;
        }
        // If data doesn't match, replace packet in cache
        else {
            ph->isValid = 1;
            packetCache[bucket] = ph;
            return 0;
        }
    }
    // Add packet to cache if entry is empty 
    else {
        ph->isValid = 1;
        packetCache[bucket] = ph;
    }
    return 0;
}
