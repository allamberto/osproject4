all: threadedRE 

threadedRE: threadedRE.o
	gcc threadedRE.o -o threadedRE -pthread -lm

threadedRE.o: threadedRE.c
	gcc -Wall -std=c99 -c threadedRE.c -o threadedRE.o 

clean:
	rm -f threadedRE.o threadedRE
