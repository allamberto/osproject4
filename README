Patrick (Ducky) Bouchon, Sophie Johnson, Rayyan Karim, Ana Luisa Lamberto
OS Project 04
Team RockOn
3/28/19

Our code begins by reading in command line arguments to understand the level the
user wants to run the code in, the number of threads to use, and the files to 
look at. Then, one thread is called as a producer and the rest of the threads are 
called as consumers. The producer thread loops through the files given by the 
user one at a time, and processes each one. The processing of a file involves 
reading through all the packets in the file one at a time and putting each on 
the buffer. Meanwhile, the consumers are waiting on the other side of the 
buffer, ready to process any packet as it appears. If the program is in level 1,
each packet is processed as a whole chunk, meaning the entire packet after the 
first 52 bytes is hashed and checked against the hash to check for redundancies.
In level 2, the packet is read through a series of 64 byte windows, again 
checking the hash of each against the cache to see how many bytes are considered 
to be redundant. Our replacement policy for level 1 is the infamous shift & evict tactic 
invented by yours truly. For the shift & evict, level 1 counts the data 
stored until it reaches 64 MB. At this point, it will evict packets starting from 
the top of the cache and moving down, flipping a coin to determine 
eviction when finding the first filled bucket. Our strategy for level 2 
is to randomly replace in the cache on the flip of a coin if the bucket is the 
same but that data stored is different. The total number of redundant packets or 
bytes is measured and reported for both level 1 and level 2 after every packet 
from every file has been processed.

We achieve optimal results when called with 4 threads (3 consumers, 1 producer).
